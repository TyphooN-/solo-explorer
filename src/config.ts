let global : any = typeof window !== 'undefined' ? window : self;
global.config = {
	apiUrl:typeof window !== 'undefined' && window.location ? window.location.href.substr(0,window.location.href.lastIndexOf('/')+1)+'api/' : 'https://www.masariwallet.com/api/',
	mainnetExplorerUrl: "https://explorer.minesolo.com/",
	testnetExplorerUrl: "http://testnet.explorer.minesolo.com/",
	testnet: false,
	coinUnitPlaces: 9,
	txMinConfirms: 5,         // corresponds to CRYPTONOTE_DEFAULT_TX_SPENDABLE_AGE in Monero
	txCoinbaseMinConfirms: 5, // corresponds to CRYPTONOTE_MINED_MONEY_UNLOCK_WINDOW in Monero
	addressPrefix: 13975,
	integratedAddressPrefix: 26009,
	addressPrefixTestnet: 13976,
	integratedAddressPrefixTestnet: 26112,
	subAddressPrefix: 23578,
	subAddressPrefixTestnet: 27947,
	feePerKB: new JSBigInt('400000000'),//20^10 - for testnet its not used, as fee is dynamic.
	dustThreshold: new JSBigInt('1000000000'),//10^10 used for choosing outputs/change - we decompose all the way down if the receiver wants now regardless of threshold
	defaultMixin: 13, // default value mixin

	idleTimeout: 30,
	idleWarningDuration: 20,

	coinSymbol: 'XSL',
	openAliasPrefix: "xsl",
	coinName: 'Solo',
	coinUriPrefix: 'solo:',
	avgBlockTime: 60,
	maxBlockNumber: 500000000,
};
